<?php
/**
 * Created by PhpStorm.
 * User: Charly-pc
 * Date: 2/05/19
 * Time: 17:40
 */

header('Content-type:application/xls');
header('Content-Disposition: attachment; filename=pointgates.xls');

require_once "conexion.php";
$conexion=conexion();
$sql = "SELECT * FROM prueba_pg ";
$g_result = $conexion->query($sql);
?>
<table border="2" >
    <tr style="background-color:green">
        <th>ID</th>
        <th>URL</th>
        <th>NOMBRE POINTGATE</th>
        <th>CLIENTE</th>
        <th>ESTATUS</th>
        <th>LOCACION</th>
    </tr>
    <?php
    while ($sql=mysqli_fetch_assoc($g_result)) {
        ?>
        <tr>
            <td><?php echo $sql['id']; ?></td>
            <td><?php echo $sql['url']; ?></td>
            <td><?php echo $sql['nombre_pg']; ?></td>
            <td><?php echo $sql['id_client']; ?></td>
            <td><?php echo $sql['estatus']; ?></td>
            <td><?php echo $sql['id_location']; ?></td>
        </tr>

    <?php
    }
    ?>
</table>
