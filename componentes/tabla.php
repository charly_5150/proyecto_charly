
<?php 
	session_start();
	require_once "../php/conexion.php";
	$conexion=conexion();
 ?>
<div class="row" xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
	<div class="col-sm-12">
<img src="imagenes/po2.png" border=0 style="width:330px; height:90px; top: 0; left: 0; background-attachment: fixed; background-repeat: no-repeat;">
	<font face="stencil" align="center" color="black"> <h1> MONITOREO DE POINTGATES</h1></font>

		<table  class="table table-hover table-condensed table-bordered" id="tabladinamicaload" >
		<caption>

			<button class="btn btn-primary" data-toggle="modal" data-target="#modalNuevo">
				Agregar Nuevo
				<span class="glyphicon glyphicon-plus"></span>
			</button>

            <button class="btn btn-primary" data-toggle="modal" >
                <a href="php/Excel.php" class="button"  style="color:#f5f5f5"> </a>
                Exportar Excel
            </button>

			<button class="btn btn-primary" data-toggle="modal" >
                <a href="librerias/pdfDomPdf/crearPdf.php" class="button"  style="color:#f5f5f5";</a>
                Exportar PDF
            </button>

		</caption>
		<thead>
			<tr>
				<td style="display:none;">#ID</td>
				<th>Nombre</th>
				<td style="display:none;">Cliente</td>
                <th>Nombre-cliente</th>
                <th>Estatus 1=ON,0=OFF</th>
                <td style="display:none;">Location</td>
		        <td>Nombre-Locacion</td>
                <td>Editar</td>
				<td>Eliminar</td>
			</tr>
           </thead>
		   <tbody>

			<?php
				if(isset($_SESSION['consulta'])){
					if($_SESSION['consulta'] > 0){
						$idp=$_SESSION['consulta'];
						$sql="select prueba_pg.*,IFNULL(locations.name,'Sin Locacion') as name,IFNULL(clients.name,'sin cliente')as cliente
                               from prueba_pg
                               left join locations on prueba_pg.id_location = locations.id_location
                               left join clients on prueba_pg.id_client = clients.id_client  where id='$idp'";
					}else{
						$sql="select prueba_pg.*,IFNULL(locations.name,'Sin Locacion') as name,IFNULL(clients.name,'sin cliente')as cliente
from prueba_pg
left join locations on prueba_pg.id_location = locations.id_location
left join clients on prueba_pg.id_client = clients.id_client";
					}
				}else{
						$sql="select prueba_pg.*,IFNULL(locations.name,'Sin Locacion') as name,IFNULL(clients.name,'sin cliente')as cliente
from prueba_pg
left join locations on prueba_pg.id_location = locations.id_location
left join clients on prueba_pg.id_client = clients.id_client ";
				}

				$result=mysqli_query($conexion,$sql);
				while($ver=mysqli_fetch_row($result)){ 

					$datos=$ver[0]."||".
						   $ver[1]."||".
						   $ver[2]."||".
						   $ver[3]."||".
						   $ver[4]."||".
						   $ver[5]."||".
                           $ver[6]."||".
                           $ver[7];
				
						   $f1="imagenes/4026425-512.png";
						   $f2="imagenes/5150.png";

						   $est=null;
                           if ($ver[4] == 0) {
                                 $est=$f2;
                                 } else {
                                 $est=$f1;
                            }


			 ?>

			<tr>
				<td style="display:none;"><?php echo $ver[0] ?></td>
				<td><?php echo "<br><a href=\"$ver[1]\">".$ver[2]."</a>";?></td>
                <td style="display:none;"><?php echo $ver[3] ?></td>
                <td><?php echo $ver[7] ?></td>
				<td><?php echo "<img src= ".$est." width='80' height='70' class='center'/><div style='visibility: hidden'>$ver[4]</div>";$ver[4]?></td>
                <td style="display:none;"><?php echo $ver[5] ?></td>
			    <td><?php echo $ver[6] ?></td>


                <td>
					<button class="btn btn-warning glyphicon glyphicon-pencil" data-toggle="modal" data-target="#modalEdicion" onclick="agregaform('<?php echo $datos ?>')">
						
					</button>
				</td>
				<td>
					<button class="btn btn-danger glyphicon glyphicon-remove" 
					onclick="preguntarSiNo('<?php echo $ver[0] ?>')">
						
					</button>
				</td>
			</tr>
			<?php 
		}
			 ?>
			 </tbody>
		</table
	</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    $('#tabladinamicaload thead th ').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder= "Buscar:' +title+'" />' );

    } );
    // DataTable
    var table = $('#tabladinamicaload').DataTable({
		language:{
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
		}
	}
	});
    // Apply the search
    table.columns().every( function () {
        var that = this;
        $( 'input', this.header() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );
} );
</script>




