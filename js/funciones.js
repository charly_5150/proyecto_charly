
function agregardatos(url,nombre_pg,id_client,estatus,id_location){
	cadena= "url=" + url +"&nombre_pg=" + nombre_pg + "&id_client=" + id_client + "&estatus=" + estatus + "&id_location="  + id_location;


	$.ajax({
		type:"POST",
		url:"php/agregarDatos.php",
		data:cadena,
		success:function(r){
			if(r){
				$('#tabla').load('componentes/tabla.php');
				alertify.success("agregado con exito :)");
			}else{
				alertify.error("Fallo el servidor :(");
			} 
		}
	});

}

function agregaform(datos){

	d=datos.split('||');

    $('#idpersona').val(d[0]);
	$('#urlu').val(d[1]);
	$('#nombre_pgu').val(d[2]);
	$('#id_clientu').val(d[3]);
	$('#estatusu').val(d[4]);
	$('#id_locationu').val(d[5]);
}

function actualizaDatos(){

    id=$('#idpersona').val();
	url=$('#urlu').val();
	nombre_pg=$('#nombre_pgu').val();
	id_client=$('#id_clientu').val();
	estatus=$('#estatusu').val();
    id_location=$('#id_locationu').val();

    cadena= "id=" + id+
        "&url=" + url +
        "&nombre_pg=" + nombre_pg +
        "&id_client=" + id_client+
        "&estatus=" + estatus+
        "&id_location=" + id_location;

    $.ajax({
		type:'POST',
		url:"php/actualizaDatos.php",
		data:cadena,
		success:function(r){
            if(r){
				$('#tabla').load('componentes/tabla.php');
				alertify.success("Actualizado con exito :)");
			}else{
				alertify.error("Fallo el servidor :(");

			}
		}
	});

}

function preguntarSiNo(id){
	alertify.confirm('Eliminar Datos', '¿Esta seguro de eliminar este registro?', 
					function(){ eliminarDatos(id) }
                , function(){ alertify.error('Se cancelo')});
}

function eliminarDatos(id){

	cadena="id=" + id ;

		$.ajax({
			type:"POST",
			url:"php/eliminarDatos.php",
			data:cadena,
			success:function(r){
				if(r==1){
					$('#tabla').load('componentes/tabla.php');
					alertify.success("Eliminado con exito!");
				}else{
					alertify.error("Fallo el servidor :(");
				}
			}
		});
}


