<?php 
  session_start();
  unset($_SESSION['consulta']);
?>
 
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" charset="utf-8">
	<title>Tabla dinamica</title>
	<link rel="stylesheet" type="text/css" href="librerias/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="librerias/alertifyjs/css/alertify.css">
	<link rel="stylesheet" type="text/css" href="librerias/alertifyjs/css/themes/default.css">
    <link rel="stylesheet" type="text/css" href="librerias/select2/css/select2.css">
    <link rel="stylesheet" type="text/css" href="librerias/datatable/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="librerias/datatable/dataTables.bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="librerias/datatable/jquery.dataTables.min2.css">

    <script src="librerias/query/jquery-3.2.1.min.js"></script>
    <script src="js/funciones.js"></script>
	<script src="librerias/bootstrap/js/bootstrap.js"></script>
	<script src="librerias/alertifyjs/alertify.js"></script>
    <script src="librerias/select2/js/select2.js"></script>
    <script src="librerias/datatable/jquery.dataTables.min.js"></script>
	<script src="librerias/datatable/dataTables.bootstrap.min.js"></script>
  
</head>

<style type="text/css">
    body {
        background:url('imagenes/green.jpg') repeat 0 0;
    }
</style>

	<div class="container">
    <div id="buscador"></div>
	<div id="tabla"></div>
	</div>

	<!-- Modal para registros nuevos -->


<div class="modal fade" id="modalNuevo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Agrega Nuevo Pointgate</h4>
      </div>
      <div class="modal-body" >

         	<div> Url: <input type="text" name="" id="url" class="form-control input-sm"></div>
        	<div>Nombre: <input name="nombre" id="nombre_pg" class="form-control input-sm"></div>

        	<div> Cliente: <select id = "id_clientes" name="id_clientes" class = "form-control" >
              <option value = "">Selecciona un cliente</option>
              <?php
              require_once "php/conexion.php";
              $conexion=conexion();

              $sql = "SELECT id_client, name FROM clients ORDER BY name ASC  ";
              $g_result = $conexion->query($sql);
              while($row = $g_result->fetch_assoc()){
                  echo "<option value ='".$row['id_client']."' >";
                  echo utf8_encode ($row['name']);
                  echo "</option>";
              }
              ?>

          </select></div>

            <div>Locacion: <select  id = "locations" name="locations"  class = "form-control" >
              <option value = "0">Selecciona una location</option>

          </select></div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal" id="guardarnuevo">
        Agregar
        </button>
       
      </div>
    </div>
  </div>
</div>

<!-- Modal para edicion de datos -->

<div class="modal fade" id="modalEdicion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Actualizar datos</h4>
      </div>
      <div class="modal-body">
      		<input type="text" hidden="" id="idpersona" name="">

          <div> Url: <input type="text" name="" id="urlu" class="form-control input-sm"></div>
          <div>Nombre: <input name="nom" id="nombre_pgu" class="form-control input-sm"></div>

          <div> Cliente: <select id = "id_clientu" name="cliente" class = "form-control" >
                  <option value = "">Selecciona un cliente</option>
                  <?php
                  require_once "php/conexion.php";
                  $conexion=conexion();

                  $sql = "SELECT id_client, name FROM clients ORDER BY name ASC  ";
                  $g_result = $conexion->query($sql);
                  while($row = $g_result->fetch_assoc()){
                      echo "<option value ='".$row['id_client']."' >";
                      echo utf8_encode ($row['name']);
                      echo "</option>";
                  }
                  ?>
              </select></div>
            <div>locacion:
            <select  name="location" id="id_locationu" class="form-control input-sm">
              <option value = "0"> Primero Selecciona un cliente</option>
            </select></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" id="actualizadatos" data-dismiss="modal">Actualizar</button>
      </div>
    </div>
  </div>
</div>
</body>
</html>
<script type="text/javascript">

	$(document).ready(function(){
		$('#tabla').load('componentes/tabla.php');

	});	
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#guardarnuevo').click(function(){
          url=$('#url').val();
          nombre_pg=$('#nombre_pg').val();
          id_client=$('#id_clientes').val();
          estatus=$('#estatus').val();
          id_location=$('#locations').val();

            agregardatos(url,nombre_pg,id_client,estatus,id_location);
        });
    });
</script>
<script language="javascript">
    //function select agregar
    $(document).ready(function(){
        $("#id_clientes").change(function () {

            $("#id_clientes option:selected").each(function () {
                id_client = $(this).val();
                $.post("php/select_client.php", { id_client: id_client }, function(data){
                    $("#locations").html(data);
                });
            });
        })
    });

</script>
<script language="javascript">
    //function select edit
    $(document).ready(function(){
        $("#id_clientu").change(function () {

            $("#id_clientu option:selected").each(function () {
                id_client = $(this).val();
                $.post("php/select_client.php", { id_client: id_client }, function(data){
                    $("#id_locationu").html(data);
                });
            })
        });
    $('#actualizadatos').click(function(){
        actualizaDatos();
    });
    });
</script>

